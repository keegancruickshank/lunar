import { Injectable } from '@angular/core';
import { Gamer } from '../classes/gamer';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public users: Array<Gamer> = [];

  constructor() {
    let user1 = new Gamer(1, "keg198", "Keegan", "Cruickshank", "Snowdown");
    let user2 = new Gamer(2, "betabot", "Matthew", "Phelan", "Soraka");
    this.users.push(user1);
    this.users.push(user2);
    console.log(this.users)
  }
}
