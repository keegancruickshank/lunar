import { Component, OnInit } from '@angular/core';
import { Gamer } from "../../classes/gamer";
import { UsersService } from "../../providers/users.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public credentials: Credential = new Credential("", "");
  public avatar = "./assets/company.png";

  constructor(private userService: UsersService) {
  }

  ngOnInit() {
    this.startAnimationSequence();
  }

  login() {
    console.log(this.credentials);
  }

  usernameChanged() {
    this.avatar = "./assets/company.png";
    for (let user in this.userService.users) {
      if (this.credentials.username == this.userService.users[user].username && this.userService.users[user].avatar != null) {
        let companyLogo = document.getElementById("company-logo");
        this.avatar = "./assets/avatars/" + this.userService.users[user].avatar + ".png";
        companyLogo.className = "animated bounceInDown";
        setTimeout(function() {
          companyLogo.className = "";
        }, 2000)
      }
    }
  }

  startAnimationSequence() {
    let companyLogo = document.getElementById("company-logo");
    companyLogo.style.display = "none";
      setTimeout(function() {
        let loader = document.getElementById("loader");
        loader.style.display = "none";
        companyLogo.style.display = "block";
        companyLogo.className = "animated fadeIn";
        setTimeout(function() {
          let loginForm = document.getElementById("form-login");
          loginForm.style["justify-content"] = "flex-start";
          loginForm.className = "form-login form-login-open";
          setTimeout(function() {
              let loginFields = document.getElementById("login-fields");
              loginFields.style.display = "flex";
              loginFields.className = "login-fields animated fadeIn";
          }, 250)
        }, 1000)
    }, 2000)
  }

}

class Credential {
  constructor(
    public username: string,
    public password: string,
  ) {}
}
